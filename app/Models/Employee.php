<?php

namespace App\Models;

use App\Traits\HasParentModel;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    /*use HasParentModel;

    protected $guarded = [];

    protected $table = 'users';

    protected $with = 'profile';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($query) {
            $query->whereHas('profile', function($query){
                $query->where('user_type', Employee::class);
            });
        });

    }*/

    /*public function profile()
    {
        return $this->morphOne(Profile::class, 'user');
    }*/
    public function user()
    {
        return $this->morphOne(User::class, 'profile');
    }

}
