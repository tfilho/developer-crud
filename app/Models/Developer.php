<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\HasParentModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Developer extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];

    protected $with = 'user';

    public function scopeApplyFilters($query)
    {

        if(request('search')){

            $value = request('search');
            $where = '%'.$value.'%';
            $query->where('hobby','like', $where)
            ->orWhere(function($query) use($value, $where){
                $query->whereHas('user', function($query) use($value, $where) {
                    $query->where('name','like',$where)
                        ->orWhere('sex','like',$where)
                        ->orWhere('birthdate','like',$where);
                });
            });
        }

        return $query;
    }

    public function user()
    {
        return $this->morphOne(User::class, 'profile');
    }
}
