<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user()
    {
        return $this->morphTo();
    }

    public function scopeIsDeveloper($query)
    {
        return $query->where('user_type',Developer::class);
    }
}
