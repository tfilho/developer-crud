<?php

namespace App\Contracts;

use App\DTO\DeveloperStoreDTO;

interface ServiceInterface
{
    public function create(DeveloperStoreDTO $developerStoreDTO);
}
