<?php

namespace App\Services;

use App\Contracts\ServiceInterface;
use App\DTO\DeveloperIndexDTO;
use App\DTO\DeveloperStoreDTO;
use App\DTO\DeveloperUpdateDTO;
use App\Models\Developer;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class DeveloperService implements ServiceInterface
{

    protected int $per_page = 7;

    public function all(DeveloperIndexDTO $developerIndexDTO){

        return Developer::applyFilters()->paginate($developerIndexDTO->per_page ?? $this->per_page);
    }

    //todo create DTO CLASS
    public function create(DeveloperStoreDTO $developerStoreDTO)
    {
        return DB::transaction(function() use ($developerStoreDTO){

            $developer = Developer::create([
                'hobby' => $developerStoreDTO->hobby,
            ]);

            $user =  User::create([
                'name' => $developerStoreDTO->name,
                'sex' => $developerStoreDTO->sex,
                'birthdate' => $developerStoreDTO->birthdate,
            ]);

            $developer->user()->save($user);

            return $this->developerById($developer->id);
        });
    }

    public function developerById(int $developer_id)
    {
        return Developer::findOrFail($developer_id);
    }

    public function update(int $developer_id, DeveloperUpdateDTO $developerUpdateDTO)
    {
        $developer = Developer::findOrFail($developer_id);

        $developer->user()->update([
            'name' => $developerUpdateDTO->name,
            'sex' => $developerUpdateDTO->sex,
            'birthdate' => $developerUpdateDTO->birthdate,
        ]);

        $developer->update([
            'hobby' => $developerUpdateDTO->hobby,
        ]);

        return $developer->refresh();
    }

    public function destroy(int $developer_id)
    {
        return Developer::findOrFail($developer_id)->delete();
    }
}
