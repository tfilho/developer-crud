<?php

namespace App\DTO;

use Spatie\DataTransferObject\DataTransferObject;
use Symfony\Component\HttpFoundation\Request;

class DeveloperStoreDTO extends DataTransferObject
{

    public static function fromRequest(Request $request): self
    {
        return new self([
            'name' => $request->input('name'),
            'sex' => $request->input('sex'),
            'birthdate' => $request->input('birthdate'),
            'hobby' => $request->input('hobby'),
        ]);
    }

    public static function fromArray(array $attributes): self
    {
        return new self([
            'name' => $attributes['name'],
            'sex' => $attributes['sex'],
            'birthdate' => $attributes['birthdate'],
            'hobby' => $attributes['hobby'],
        ]);
    }

    public $name;

    public $sex;

    public $birthdate;

    public $hobby;
}
