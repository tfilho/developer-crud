<?php

namespace App\DTO;

use Spatie\DataTransferObject\DataTransferObject;
use Symfony\Component\HttpFoundation\Request;

class DeveloperUpdateDTO extends DataTransferObject
{
    public static function fromRequest(Request $request): self
    {
        return new self([
            'name' => $request->input('name'),
            'sex' => $request->input('sex'),
            'birthdate' => $request->input('birthdate'),
            'hobby' => $request->input('hobby'),
        ]);
    }

    public $name;

    public $sex;

    public $birthdate;

    public $hobby;
}
