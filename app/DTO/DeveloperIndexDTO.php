<?php

namespace App\DTO;

use Spatie\DataTransferObject\DataTransferObject;
use Symfony\Component\HttpFoundation\Request;

class DeveloperIndexDTO extends DataTransferObject
{

    public static function fromRequest(Request $request): self
    {
        return new self([
            'per_page' => $request->input('per_page'),
            'search' => $request->input('search'),
        ]);
    }

    public $per_page;

    public $search;
}
