<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class DeveloperStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required|string|min:5|max:255',
            'sex' => 'required|string|in:F,M,O|size:1',
            'birthdate' => 'required|date_format:Y-m-d|before:16 years ago',
            'hobby' => 'required|string|min:3|max:255'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'birthdate' => Carbon::parse($this->birthdate)->format('Y-m-d'),
        ]);
    }
}
