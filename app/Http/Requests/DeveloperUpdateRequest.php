<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeveloperUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|string|min:5|max:255',
            'sex' => 'sometimes|string|in:F,M,O|size:1',
            'birthdate' => 'sometimes|date|before:16 years ago',
            'hobby' => 'sometimes|string|min:3|max:255'
        ];
    }
}
