<?php

namespace App\Http\Controllers;

use App\DTO\DeveloperIndexDTO;
use App\DTO\DeveloperStoreDTO;
use App\DTO\DeveloperUpdateDTO;
use App\Http\Requests\DeveloperStoreRequest;
use App\Http\Requests\DeveloperUpdateRequest;
use App\Models\Developer;
use App\Services\DeveloperService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Ramsey\Uuid\Type\Integer;

class DeveloperController extends Controller
{

    /**
     * @var DeveloperService
     */
    private $developerService;

    /**
     * @param DeveloperService $developerService
     */
    public function __construct(DeveloperService $developerService)
    {
        $this->developerService = $developerService;
    }

    public function index(Request $request)
    {
        $developers = $this->developerService->all(DeveloperIndexDTO::fromRequest($request));

        return Response::json(
            [
                'success' => true,
                'developers' => $developers,
            ], \Symfony\Component\HttpFoundation\Response::HTTP_OK
        );
    }

    public function store(DeveloperStoreRequest $request)
    {
        try {

            $developer = $this->developerService->create(DeveloperStoreDTO::fromRequest($request));

            return Response::json(
                [
                    'success' => true,
                    'message' => 'Developer was created',
                    'developer' => $developer->ToArray(),
                ], \Symfony\Component\HttpFoundation\Response::HTTP_CREATED
            );

        }catch (\Exception $exception){

            Log::info($exception->getMessage());

            return Response::json(
                [
                    'success' => false,
                    'code' => $exception->getCode(),
                ], \Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST
            );
        }
    }

    public function show(int $developer_id)
    {

        $developer = $this->developerService->developerById($developer_id);

        return Response::json(
            [
                'success' => true,
                'developer' => $developer->ToArray(),
            ], \Symfony\Component\HttpFoundation\Response::HTTP_OK
        );
    }

    public function update(DeveloperUpdateRequest $request, int $developer_id)
    {
        try {

            $developer = $this->developerService->update($developer_id, DeveloperUpdateDTO::fromRequest($request));

            if ($developer instanceof Developer) {

                return Response::json(
                    [
                        'success' => true,
                        'message' => 'Developer was updated',
                        'developer' => $developer->ToArray(),
                    ], \Symfony\Component\HttpFoundation\Response::HTTP_OK
                );
            }
        } catch (\Exception $exception) {

            Log::info($exception->getMessage());

            return Response::json(
                [
                    'success' => false,
                    'code' => $exception->getMessage(),
                ], \Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST
            );
        }
    }

    public function destroy(int $developer_id)
    {
        try {

            if ($this->developerService->destroy($developer_id)) {
                return Response::json(
                    [
                        'success' => true,
                        'message' => 'Developer was deleted',
                    ], \Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT
                );
            }
            return Response::json(
                [
                    'success' => false,
                    'message' => 'It was not possible to delete',
                ], \Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST
            );
        } catch (\Exception $exception) {
            return Response::json(
                [
                    'success' => false,
                    'code' => $exception->getCode(),
                ], \Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST
            );
        }
    }
}
