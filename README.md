# Developers CRUD

## About

Developers CRUD is a very simple developer database.

## How to Install
#### Install the dependencies
`composer install`

#### Create the Enviroment
`cp .env.example .env`

#### Create a security key for the application
`php artisan key:generate`

#### Run the Migrations
`php artisan migrate:fresh`

#### Run the Seeders
`php artisan db:seed`

#### Test
`php artisan test`
