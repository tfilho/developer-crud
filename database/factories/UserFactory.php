<?php

namespace Database\Factories;

use App\Models\Developer;
use App\Models\Sex;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'sex' => $this->sex(),
            'birthdate' => $this->birthdate(),
            /*'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),*/
            /*'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),*/
        ];
    }

    private function sex()
    {
        return $this->faker->randomElement([
            Sex::FEMALE,
            Sex::MALE,
        ]);
    }

    private function birthdate(){

        return $this->faker->date('Y-m-d', Carbon::now()->addYears(-16));
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
