<?php

namespace Database\Factories;

use App\Models\Developer;
use App\Models\Sex;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DeveloperFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Developer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'hobby' => $this->hobby(),
        ];
    }

    private function hobby()
    {
        return $this->faker->randomElement([
            'Reading',
            'Gardening',
            'Play Piano',
            'Play Guitar',
            'Photography',
            'Cooking',
            'Drawing',
            'Chess',
            'Hiking',
            'Dance',
            'Cycling',
            'Knitting',
        ]);
    }
}
