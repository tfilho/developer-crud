<?php

namespace Database\Seeders;

use App\Models\Developer;
use App\Models\Employee;
use App\Models\Profile;
use App\Models\Sex;
use App\Models\User;
use Illuminate\Database\Seeder;
use Ramsey\Collection\Collection;

class DeveloperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        User::factory(1)->for(
            Developer::factory(), 'profile'
        )->create();
    }
}
