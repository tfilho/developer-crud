<?php

namespace Tests\Unit;

use App\DTO\DeveloperStoreDTO;
use App\Models\Developer;
use App\Models\Sex;
use App\Services\DeveloperService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use PHPUnit\Framework\TestCase;
use Tests\Traits\DeveloperFactoryTrait;

class DeveloperTest extends TestCase
{
    use DatabaseMigrations, DeveloperFactoryTrait;

    /**
     * @test
     */
    public function it_creates_a_new_developer() : void
    {
        //$developer = $this->makeDeveloper();

        $developerService = $this->getMockBuilder(DeveloperService::class)
            //->disableOriginalConstructor()
            //->setMockClassName('create')
            ->onlyMethods(['create'])
            ->getMock();

        $attributes = [
            'name' => 'Tarcisio Filho',
            'sex' => Sex::MALE,
            'birthdate' => '1989-03-08',
            'hobby' => 'Tocar Piano',
        ];

        $developerService->expects($this->once())
            ->method('create')
            ->with($this->equalTo(DeveloperStoreDTO::fromArray($attributes)))
            ->willReturn(Developer::class);

        $developerService->create(DeveloperStoreDTO::fromArray($attributes));

        //dd($service->create(DeveloperStoreDTO::fromArray($this->makeDeveloper())));
        //$this->developerService->create(DeveloperStoreDTO::fromArray($this->makeDeveloper()));

        //$this->assertInstanceOf(Developer::class,'');
    }

}
