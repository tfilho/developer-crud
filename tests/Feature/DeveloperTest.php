<?php

namespace Tests\Feature;

use App\Models\Developer;
use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response as ResponseHttp;
use Tests\TestCase;
use Tests\Traits\DeveloperFactoryTrait;

class DeveloperTest extends TestCase
{
    Use DatabaseMigrations, DeveloperFactoryTrait;

    const AMOUNT_DEVELOPERS = 10;
    const NON_EXISTENT_DEVELOPER_ID = 1;

    /**
     * ActionVerb_WhoOrWhatToDo_ExpectedBehavior
     * */

    /**
     * @test
     */
    public function it_fetches_developers()
    {
        $this->createDevelopers(DeveloperTest::AMOUNT_DEVELOPERS);

        $this->getJson('/api/developers')
            ->assertOk()
            ->assertJsonFragment(['success' => true]);

    }

    /**
     * @test
     */
    public function it_fetches_a_single_developer()
    {
        $developer = $this->createDeveloper();

        $this->getJson('/api/developers/'. $developer->id)
            ->assertOk()
            ->assertJsonFragment(['success' => true]);
    }

    /**
     * @test
     */
    public function it_creates_a_new_developer()
    {
        $developerAttributes = $this->makeDeveloper();

        $this->postJson('/api/developers/', $developerAttributes)
            ->assertCreated();
    }

    /**
     * @test
     */
    public function it_404s_if_a_developer_not_found()
    {
        $this->getJson('/api/developers/'. DeveloperTest::NON_EXISTENT_DEVELOPER_ID)
            ->assertNotFound();
    }

    /**
     * @test
    */
    public function it_throws_a_422_if_a_new_developer_request_fails_validation()
    {
        $this->postJson('/api/developers/')
            ->assertUnprocessable();
    }

    /**
     * @test
     */
    public function it_updates_an_existent_developer()
    {
        $developer = $this->createDeveloper();

        $attributes = [
            'hobby' => 'Walk in the park',
        ];

        $this->putJson('/api/developers/'. $developer->id , $attributes)
            ->assertOk();
    }
}
