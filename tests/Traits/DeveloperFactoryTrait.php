<?php

namespace Tests\Traits;

use App\Models\Developer;
use App\Models\User;

trait DeveloperFactoryTrait
{
    protected function makeDeveloper()
    {
        return $this->makeDevelopers(1)
            ->first()
            ->toArray();
    }

    protected function makeDevelopers(int $amount)
    {
        return User::factory()
            ->count($amount)
            ->make()
            ->map(function($user){
                $user->hobby =  Developer::factory()->count(5)->make()->first()->hobby;
                return $user;
            });
    }


    protected function createDeveloper()
    {
        return $this->createDevelopers(1)->first();
    }

    protected function createDevelopers(int $amount)
    {
        return User::factory()
            ->count($amount)
            ->create()
            ->map(function($user){
                $user->hobby =  Developer::factory()->count(1)->create()->first()->hobby;
                return $user;
            });
    }
}
