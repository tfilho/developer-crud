import Vue from 'vue';
import VueRouter from 'vue-router';

import AllDevelopers from './components/developer/Index';
import AddDeveloper from './components/developer/Add';
import EditDeveloper from './components/developer/Edit';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: [
        {
            name: 'home',
            path: '/',
            component: AllDevelopers
        },
        {
            name: 'add',
            path: '/add',
            component: AddDeveloper
        },
        {
            name: 'edit',
            path: '/edit/:id',
            component: EditDeveloper
        }
    ]
});

export default router;

