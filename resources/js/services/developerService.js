import axios from "axios";

class DeveloperService {

    url = "//" + window.location.hostname + "/api/developers/";

    getAll(page = 1, search = '') {
        return axios.get(`${this.url}?page=${page}&search=${search}`);
    }

    get(id) {
        return axios.get(`${this.url}${id}`);
    }

    create(data) {
        return axios.post(this.url, data);
    }

    update(id, data) {
        return axios.put(`${this.url}${id}`, data);
    }

    delete(id) {
        return axios.delete(`${this.url}${id}`);
    }

    deleteAll() {
        return axios.delete(this.url);
    }

    findByAge(age) {
        return axios.get(`${this.url}?age=${age}`);
    }
}

export default new DeveloperService();
